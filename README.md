# EFK Stack

Elasticsearch, Fluentd and Kibana stack deployed to Kubernetes with Kustomize.

## Warning

For the moment this expects you to use namespace kube-logging because it's hardcoded in fluentd.yaml for a ClusterRoleBinding. Can always be patched away in an overlay if you want.

